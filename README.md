# README #

This is a repo to share my source files from a project (CSE 6010 Computational Problem Solving) at Georgia Institute of Technology.

The objective of the project was to analyze the social influence of researchers in the field of network theory by evaluating the centrality of these researchers from a publicly available dataset of coauthorship graph.

There are two separate programs, one for converting the dataset into a different format and the other to analyze the dataset. I have also shared an interesting assignment on discrete event simulation that I did as part of the same course.

All the programs were written in C.