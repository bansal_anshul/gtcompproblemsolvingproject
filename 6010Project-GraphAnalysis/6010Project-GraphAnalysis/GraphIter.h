//
//  GraphIter.h
//  GraphAnalysis
//
//  Created by Anshul Bansal on 10/22/15.
//  Copyright © 2015 Anshul Bansal. All rights reserved.
//

#ifndef GraphIter_h
#define GraphIter_h

#include <stdio.h>
#include <stdlib.h>

// Struct to hold the graph
typedef struct Graph Graph;

// Initialization for graph
Graph* create_graph(int N);

// Add an edge to the graph given by source--to
void addElement (Graph *g, unsigned source, unsigned to, double weight);

/**
 *  Get the size of the grah
 *
 *  @param G The graph to get size of
 *
 *  @return Number of nodes in the graph
 */
int GetSize(Graph *G);

// Find first neighbour of the element "from" given in AdjList
int first_neigh(Graph *G, unsigned from);

// Iterator over neighbours in AdjList
int next_neigh(Graph *G);

// Indicator that we have reached end of the AdjList
int done_neigh(Graph *G);

// Returns the value of the first node that isn't visited by iterator
int unvisited(Graph *G);

// Frees memory held by graph struct
void free_graph (Graph *g);

/**
 *  Get the weight of the current iter ALelement
 *
 *  @param G Graph to get this info from
 *
 *  @return Weight of the edge represented by current iter element
 */
double GetCurrWeight(Graph *G);

#endif /* GraphIter_h */
