//
//  PQHeap.c
//  6010Assignment2-2
//
//  Created by Anshul Bansal on 10/6/15.
//  Copyright © 2015 Anshul Bansal. All rights reserved.
//  Program leveraged from Assignment 2. Code developed by Anshul Bansal
//  Modified to address reallocation issues
//  Added function that reshuffles the heap if the priority is changed by calling program


#include "PQHeap.h"
#include <stdlib.h>
#include "Common.h"
#include <math.h>

int MaxHeapSize = 10;

// This file implements a MIN Priority Queue (i.e. elements with the
// smallest "priority" are removed first from the queue) using concept of Heaps implemented through an Array



//Define one element of Priority Queue as struct
typedef struct HeapNode {
    double priority;
    unsigned int node;
} HeapNode;


//Define PQ as a struct of Queue, its current size and value of last element filled
typedef struct HeapQueue {
    HeapNode* HQ;
    unsigned int size;
    unsigned int elementnumber;
} HeapQueue;



//create priority queue as array of structs (using initial maxsize parameter) and return the queue
HeapQueue* pqheap_create() {
    HeapQueue *q;
    q = (HeapQueue*) malloc(sizeof(HeapQueue));
    q->HQ = (HeapNode*) malloc(sizeof(HeapNode)*MaxHeapSize);
    if (q->HQ == NULL) FatalError("pqheap_create", "Could not allocate memory.");
    q->size = MaxHeapSize;
    q->elementnumber = 0;
    return q;
}


//Move the inserted item up the queue to maintain heap property
void heapify_up(int i, HeapQueue* q) {
    //if we are at the top of the heap then return
    if (i==0) {
        return;
    }
    //if priority of element is lower than parent then swap elements
    if (q->HQ[i].priority < q->HQ[(i-1)/2].priority) {
        struct HeapNode temp;
        temp.priority = q->HQ[i].priority;
        temp.node = q->HQ[i].node;
        q->HQ[i].priority = q->HQ[(i-1)/2].priority;
        q->HQ[i].node = q->HQ[(i-1)/2].node;
        q->HQ[(i-1)/2].priority = temp.priority;
        q->HQ[(i-1)/2].node = temp.node;
        heapify_up((i-1)/2, q);
    }
    return;
}


//move the elements down the queue to maintain heap property
void heapify_down(int i, HeapQueue *q) {
    //if element does not have a child node
    if (2*i+1 > q->elementnumber-1) {
        return;
    }
    
    //if element has only one child node
    else if (2*i+1 == q->elementnumber - 1) {
        //if priority of parent is lower than child node
        if (q->HQ[i].priority <= q->HQ[2*i+1].priority) {
            return;
        }
        else {
            //swap parent and child
            struct HeapNode temp;
            temp.priority = q->HQ[2*i+1].priority;
            temp.node = q->HQ[2*i+1].node;
            q->HQ[2*i+1].priority = q->HQ[i].priority;
            q->HQ[2*i+1].node = q->HQ[i].node;
            q->HQ[i].priority = temp.priority;
            q->HQ[i].node = temp.node;
            return;
        }
        
    }
    
    //if priority of parent node is lower than both children nodes
    if (q->HQ[i].priority <= q->HQ[2*i+1].priority && q->HQ[i].priority <= q->HQ[2*i+2].priority) {
        return;
    }
    
    //else swap parent element with child element that has lowest priority
    if (q->HQ[2*i+1].priority < q->HQ[2*i+2].priority) {
        struct HeapNode temp;
        temp.priority = q->HQ[2*i+1].priority;
        temp.node = q->HQ[2*i+1].node;
        q->HQ[2*i+1].priority = q->HQ[i].priority;
        q->HQ[2*i+1].node = q->HQ[i].node;
        q->HQ[i].priority = temp.priority;
        q->HQ[i].node = temp.node;
        heapify_down(2*i+1, q);
    }
    
    else {
        struct HeapNode temp;
        temp.priority = q->HQ[2*i+2].priority;
        temp.node = q->HQ[2*i+2].node;
        q->HQ[2*i+2].priority = q->HQ[i].priority;
        q->HQ[2*i+2].node = q->HQ[i].node;
        q->HQ[i].priority = temp.priority;
        q->HQ[i].node = temp.node;
        heapify_down(2*i+2, q);
    }
    
}

//get current size of priority queue
unsigned int pqheap_size(HeapQueue *q) {
    if (q == NULL)
        return 0;
    return q->elementnumber;
}

//insert element into priority queue
void pqheap_insert(HeapQueue* q, double prio, unsigned int n) {
    //check for empty priority queue
    if (q == NULL || q->HQ == NULL ) {
        Warning("pqheap_insert", "No heap exists");
        exit(1);
    }
    
    //insert values of new element in last node
    q->HQ[q->elementnumber].node = n;
    q->HQ[q->elementnumber].priority = prio;
    
    //move this new element up the priority queue so that heap property is maintained
    heapify_up(q->elementnumber, q);
    q->elementnumber ++;
    
    //If pq is full then increase the size of the queue
    if (q->elementnumber == q->size) {

        q->size = q->size*2;
        HeapNode* x = realloc(q->HQ, sizeof(HeapNode)*q->size);
        
        //Check for reallocation errors
        if (x == NULL) {
            free(q->HQ);
            free(q);
            Warning("pqheap_insert", "Error while increasing size, cant realloc");
            exit(1);
        }
        
        q->HQ = x;
        
    }
    
    return;

}

//Increase the size of the priority queue by creating a new array of double the original size and
//copying elements of original array to the new array
//HeapQueue growHeap (HeapQueue Array) {
//    //HeapQueue temp = (HeapQueue) realloc(*Array, ((elementnumber+1)*sizeof(HeapQueue)));
//    
//    //create new queue with double the size
//    MaxHeapSize = MaxHeapSize*2;
//    HeapQueue temp = (HeapQueue) malloc(sizeof(struct HeapNode)*MaxHeapSize);
//    if (temp == NULL) {
//        FatalError("pqheap_create", "Could not allocate memory.");
//    }
//    else {
//        //copy elements
//        for (int i = 0; i<elementnumber; i++) {
//            temp[i].priority = Array[i].priority;
//            temp[i].data = Array[i].data;
//        }
//    }
//    return temp;
//}



//delete element from priority queue
unsigned int pqheap_delete(HeapQueue *q) {

    if (q->HQ == NULL) {
        //temp.data = NULL;
        //temp.HQ = NULL;
        return 1;
    }
    
    if (q->elementnumber == 0) {
        Warning("pqheap_delete", "No elements in heap");
        exit(1);
    }
    
    unsigned int temp = q->HQ[0].node;

    //Remove root node of heap and copy data from last element into root node
    q->HQ[0].priority = q->HQ[q->elementnumber - 1].priority;
    q->HQ[0].node  = q->HQ[q->elementnumber - 1].node;
    
    //Set priority of last element to some large finite value
    q->HQ[q->elementnumber-1].priority = INT32_MAX;
    q->HQ[q->elementnumber-1].node = INT32_MAX;
    q->elementnumber --;
    
    //Move the root element down the queue to maintain heap property
    heapify_down(0, q);
    
    //If current array is larger by more than twice the required heap, then create a new heap with 50% of the original size
    if (q->size/2 > q->elementnumber) {

        q->size = q->size/2;
        HeapNode* x = realloc(q->HQ, sizeof(HeapNode)*q->size);
        
        //Check for reallocation errors
        if (x == NULL) {
            free(q->HQ);
            free(q);
            Warning("pqheap_delete", "Error while reducing size, cant realloc");
            exit(1);
        }
        
        q->HQ = x;
    }
    return temp;
}

//Reduce the size of the priority queue by creating a new array of 75% of the original size
//and copy the elements from original array to this new array
//HeapQueue shrinkHeap (HeapQueue Array) {
//    //HeapQueue temp = (HeapQueue) realloc(*Array, ((elementnumber+1)*sizeof(HeapQueue)));
//    MaxHeapSize = (int) floor(MaxHeapSize*0.75);
//    HeapQueue temp = (HeapQueue) malloc(sizeof(struct HeapNode)*MaxHeapSize);
//    if (temp == NULL) {
//        FatalError("pqheap_create", "Could not allocate memory.");
//    }
//    else {
//        //copy elements in new array
//        for (int i = 0; i<elementnumber; i++) {
//            temp[i].priority = Array[i].priority;
//            temp[i].data = Array[i].data;
//        }
//    }
//    return temp;
//}

//Updates position of nodes that have their priority modified by calling program
void pqheap_refresh(HeapQueue* q, double prio, unsigned int n) {
    if (q==NULL) {
        Warning("pqheap_refresh", "No queue found");
        exit(1);
    }
    
    int i = 0;
    
    //Find the node in the heap
    while (q->HQ[i].node != n) {
        i++;
        if (i==q->elementnumber) {
            Warning("pqheap_refresh", "Node not found in heap");
            exit(1);
        }
    }
    
    //Update the node's priority
    double x = q->HQ[i].priority;
    q->HQ[i].priority = prio;
    
    //Send the node to its correct position. If priority decreased then node goes up, otherwise it goes down
    if (prio < x) {
        heapify_up(i, q);
    }
    
    else if (prio > x) {
        heapify_down(i, q);
    }
    
    return;
    
}

//free space from priority queue
void pqheap_free (HeapQueue *q) {
    if (q == NULL)
        return;

    free(q->HQ);
    free(q);
    
    return;
    
}

