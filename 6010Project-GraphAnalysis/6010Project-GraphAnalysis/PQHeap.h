//
//  PQHeap.h
//  6010Assignment2-2
//
//  Created by Anshul Bansal on 10/6/15.
//  Copyright © 2015 Anshul Bansal. All rights reserved.
//

#ifndef PQHeap_h
#define PQHeap_h

#include <stdio.h>

// This file implements a MIN Priority Queue (i.e. elements with the
// smallest "priority" are removed first from the queue) using concept of Heaps implemented through an Array

//Define a pointer to Heap
typedef struct HeapQueue HeapQueue;


/*
 * Create an empty priority queue as an array of structs and return a pointer to it
 * no params
 */
HeapQueue* pqheap_create();


/*
 * Push an element with a given priority into the queue.
 *
 * @param hq the pointer to a priority queue
 * @param priority the element's priority
 * @param data the data representing the element
 */
void pqheap_insert(HeapQueue* hq, double priority, unsigned int data);

/*
 * Pop the top element from the priority queue.
 *
 * @param hq the priority queue
 * @return the node representing the top element as an int value
 */
unsigned int pqheap_delete(HeapQueue* hq);

/*
 * Free the memory allocated to the priority queue.
 *
 * @param hq the priority queue
 */
void pqheap_free (HeapQueue* hq);


/*
 * Update the position of an element in the queue, whose priority has been modified
 *
 * @param hq the pointer to a priority queue
 * @param priority the element's new priority
 * @param data the data representing the element
 */
void pqheap_refresh(HeapQueue* hq, double priority, unsigned int i);


/*
 * Get the size of the priority queue
 *
 * @param hq the priority queue
 *
 * @return the size of the queue
 */
unsigned int pqheap_size(HeapQueue* hq);



#endif /* PQHeap_h */
