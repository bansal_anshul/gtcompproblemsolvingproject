//
//  main.c
//  6010Project-GraphAnalysis
//
//  Created by Anshul Bansal on 11/22/15.
//  Copyright © 2015 Anshul Bansal. All rights reserved.
//

#include <stdio.h>
#include "GraphIter.h"
#include "Dijkstra.h"
#include "StackArray.h"
#define normalize 1
#define Inf 10000.0

int main(int argc, const char * argv[]) {
    
    //Open file and read contents to create graph
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("Cant open file. Please resolve error\n");
        return 1;
    }
    
    //Get number of nodes and call function to create adjacency list
    int countNodes;
    fscanf(fp, "%d",&countNodes);
    
    //Stores the graph as adjaceny list
    Graph *G;
    
    G = create_graph(countNodes);
    
    //Array stores degree of each node
    int degarray[countNodes];
    
    
    //Array stores number of papers published by each author
    double PParray[countNodes];
    
    //Initialize
    for (int k = 0; k<countNodes; k++) {
        degarray[k] = 0;
        PParray[k] = 0;
    }
    
    unsigned source, target;
    double weight, dweight;
    //Loop through all edges and keep adding to graph structure
    //Source: https://www.cs.bu.edu/teaching/c/file-io/intro/
    while (!feof(fp)) {
        if (fscanf(fp, "%d %d %lf", &source, &target, &weight) != 3) {
            break;
        }
        //The weights in the dataset indicate strength of collaborations. Small weights mean stronger collaborations
        //Hence, distance between nodes is the inverse of these weights
        dweight = 1.0 / weight;
        addElement(G, source, target, dweight);
        addElement(G, target, source, dweight);
        
        //Calculate degree and papers published
        degarray[source] += 1;
        degarray[target] += 1;
        
        //Since every edge weight represents the contribution between two authors on all papers,
        //The sum of all edge weights for edges coming out of a node represent the number of papers
        //published by that author (for papers which had minimum of two authors)
        PParray[source] += weight;
        PParray[target] += weight;
        
    }
    
    //Close file
    fclose(fp);
    //int diameter = 0;
    
    //Calculate degree of all nodes
    
    
    //Calculate number of papers published by each researcher
    
    // Distance matrix
    double **distMatrix = (double**) malloc(sizeof(double*) * countNodes);
    
    for (size_t i = 0; i < countNodes; i++) {
        distMatrix[i] = (double*) malloc(sizeof(double) * countNodes);
    }
    
    // Betweenness Array
    double *CBarray = (double*) malloc(sizeof(double) * countNodes);
    
    // Closeness Array
    double *CCarray = (double*) malloc(sizeof(double) * countNodes);
    
    //Initialize CBarray to 0
    for (int j = 0; j < countNodes; j++) {
        CBarray[j] = 0;
        CCarray[j] = 0;
    }
    
    
    // Fill distMatrix and CBarray
    for (int i = 0; i < countNodes; i++) {
        Dijkstra(G, i, distMatrix[i], CBarray);
    }
    
    //Dijkstra(G, 0, distMatrix[0], CBarray);
    
    //Calculate Closeness using distance matrix
    for (int i = 0; i < countNodes; i++) {
        double x = 0;
        for (int j = 0; j < countNodes; j++) {
            if (i != j && distMatrix[i][j] != Inf) {

                x = x + 1.0/distMatrix[i][j];
                
            }
            
        }
        //CCarray[i] = x/(countNodes-1);
        CCarray[i] = x;
    }
    
//    for (size_t i = 0; i < countNodes; i++) {
//        for (size_t j = 0; j < countNodes; j++) {
//            if (diameter < distMatrix[i][j]) {
//                diameter = distMatrix[i][j];
//            }
//        }
//    }
//    
//    printf("Diameter: %d\n", diameter);
    
    // Writig distance matrix out to a file
    char Distfileout[50];
    sprintf(Distfileout, "%s_dist", argv[1]);
    FILE *fp2 = fopen(Distfileout, "w");
    
    for (size_t i = 0; i < countNodes; i++) {
        for (size_t j = 0; j < countNodes; j++) {
            fprintf(fp2, "%lf\t", distMatrix[i][j]);
        }
        fprintf(fp2, "\n");
    }
    
    fclose(fp2);
    
    //Writing betweenness and closeness to file
    char CBfileout[50];
    sprintf(CBfileout, "%s_bw", argv[1]);
    FILE *fp3 = fopen(CBfileout, "w");
   
    fprintf(fp3, "Node\t");
    fprintf(fp3, "Betweenness\t");
    fprintf(fp3, "Closeness\n");

    for (size_t i = 0; i < countNodes; i++) {
        fprintf(fp3, "%zu\t",i);
        fprintf(fp3, "%0.8lf\t", CBarray[i]);
        fprintf(fp3, "%0.8lf\n", CCarray[i]);
        
    }
    
    fclose(fp3);

    //Writing degree and paper publish count to file
    char PPfileout[50];
    sprintf(PPfileout, "%s_Deg", argv[1]);
    FILE *fp4 = fopen(PPfileout, "w");
    
    fprintf(fp4, "Node\t");
    fprintf(fp4, "Degree\t");
    fprintf(fp4, "Count of Papers published\n");

    for (size_t i = 0; i < countNodes; i++) {
        fprintf(fp4, "%zu\t",i);
        fprintf(fp4, "%u\t", degarray[i]);
        fprintf(fp4, "%f\n", PParray[i]);
        
    }
    
    fclose(fp4);
    
    free_graph(G);
    
    for (size_t i = 0; i < countNodes; i++) {
        free(distMatrix[i]);
    }
    free(distMatrix);
    
    free(CBarray);
    
    free(CCarray);
    
    printf("Execution Complete");
    
    
    return 0;
}