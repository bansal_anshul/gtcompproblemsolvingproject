//
//  StackArray.h
//  6010Project-GraphAnalysis
//
//  Created by Anshul Bansal on 11/29/15.
//  Copyright © 2015 Anshul Bansal. All rights reserved.
//

#ifndef StackArray_h
#define StackArray_h
#include <stdio.h>


//This program creates a LIFO (stack) using array implementation where the size of stack is pre-defined


//Struct that stores stack as array
typedef struct stack stack;


//Function to create stack
/**
 *  Create stack of pre-specified height
 *  @param n         Size of stack
 **/
stack* create_stack(int n);


/**
 *  check if stack is empty. Return 1 if not empty and 0 otherwise
 *  @param Q         The pointer to a stack that needs to be checked
 **/
int stack_empty (stack *Q);


//Function to return size of stack
/**
 *  Return size of stack
 *  @param Q         The pointer to a stack that needs to be checked
 **/
int size_stack(stack *Q);


/**
 *  Insert element to top of stack
 *  @param Q         The pointer to a stack
 *  #param node      Int value that needs to be inserted
 **/
void push_stack (stack *Q, int node);


/**
 *  Remove element form the top of stack
 *  @param Q         The pointer to a stack
 **/
int pop_stack (stack *Q);


/**
 *  Free allocated memory
 *  @param Q         The pointer to a stack
 **/
int free_stack(stack *Q);

#endif /* StackArray_h */
