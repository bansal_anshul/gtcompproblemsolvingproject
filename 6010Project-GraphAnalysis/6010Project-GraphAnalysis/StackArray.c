//
//  StackArray.c
//  6010Project-GraphAnalysis
//
//  Created by Anshul Bansal on 11/29/15.
//  Copyright © 2015 Anshul Bansal. All rights reserved.
//

#include "StackArray.h"
#include <stdlib.h>
#include "Common.h"
#include <math.h>


//This program implements Stacks (LIFO Queue) as arrays.
//For the purpose of our software the queue size will be known well in advance so array implementation
//will be assumingly more efficient compared to linked lists.

//Reference: https://www.cs.bu.edu/teaching/c/stack/array/


//Struct that stores stack as array, size of array and value of last element filled
typedef struct stack {
    int size;
    int* st;
    int top;
} stack;


//Function to create stack
stack* create_stack(int n) {
    
    //Allocate space
    stack *Queue = (stack*) malloc(sizeof(stack));
    Queue->size = n;
    Queue->top = -1;
    
    Queue->st = (int*) malloc ( sizeof(int) * n );
    
    //Error check
    if (Queue->st == NULL) {
        Warning("create_stack", "Stack not created");
        exit(1);
    }
    
    //Initialize stack
    for (int i = 0; i < Queue->size; i++) {
        Queue->st[i] = -1;
    }
    
    return Queue;
}


//Function to check if stack is empty
int stack_empty (stack *Q) {
    if (Q->top == -1) {
        return SUCCESS;
    }
    return FAIL;
}


//Function to check if stack is full
int stack_full (stack *Q) {
    if (Q->top >= Q->size - 1) {
        return SUCCESS;
    }
    return FAIL;
}


//Function to return size of stack
int size_stack(stack *Q) {
    
    return Q->size;

}



//Function to insert element in stack
void push_stack (stack *Q, int node) {
    
    //If stack is already full then cannot add more
    if (!stack_full(Q)) {
        Warning("push_stack", "Stack is full. Cannot insert!");
        exit(1);
    }
    
    //Add to front of array
    Q->top++;
    Q->st[Q->top] = node;
}


//Function to pop element from stack
int pop_stack (stack *Q) {
    if (!stack_empty(Q)) {
        Warning("pop_stack", "Stack is empty. Cannot pop!");
        exit(1);
    }

    int node;
    node = Q->st[Q->top];
    Q->top--;
    return node;
}


//Function to free the stack
int free_stack(stack *Q) {
    
    if (Q->size < 1 || Q->st == NULL)
        return 1;           // return failure
    
    free(Q->st);
    free(Q);
    
    return 0;               // return success
}

