//
//  GraphIter.c
//  GraphAnalysis
//
//  Created by Anshul Bansal on 10/22/15.
//  Copyright © 2015 Anshul Bansal. All rights reserved.
//

#include "GraphIter.h"
#include <stdlib.h>

typedef struct ALElement {
    unsigned int node;
    //unsigned int to;
    double weight;
    struct ALElement *next;
} ALElement;

typedef struct Graph {
    int numNodes;
    ALElement **adjList;
    ALElement *iter;
    int *visitedList;
} Graph;

Graph *G;
unsigned from = 2;


//Create Graph

Graph* create_graph(int N) {
    Graph *g = (Graph*) malloc(sizeof(Graph));
    
    g->numNodes = N;
    g->adjList = (ALElement**) malloc (sizeof(ALElement*)*N);
    g->iter = NULL;
    for (int i = 0; i < N; i++) {
        g->adjList[i] = (ALElement*) malloc(sizeof(ALElement));
        g->adjList[i]->node = i;
        g->adjList[i]->next = NULL;
    }
    g->visitedList = (int*)malloc(sizeof(int)*N);
    for (int i=0; i<N; i++) {
        g->visitedList[i]=0;
    }
    return g;
}

void addElement (Graph *g, unsigned source, unsigned to, double weight) {
    ALElement *p = g->adjList[source];
    while (p->next!=NULL) {
        p = p->next;
    }
    ALElement* Element = (ALElement*) malloc((sizeof(ALElement)));
    Element->node = to;
    Element->weight = weight;
    Element->next = NULL;
    p->next = Element;
}

int GetSize(Graph *G)
{
    return G->numNodes;
}

int first_neigh(Graph *G, unsigned from) {
    G->iter = G->adjList[from]->next;
    if (G->iter==NULL) {
        return -1;
    }
    if (!G->iter) {
        return -1;
    }
    G->visitedList[from] = 1;
    G->visitedList[G->iter->node] = 1;
    return G->iter->node;
}

int next_neigh(Graph *G) {
    G->iter = G->iter->next;
    // In case of a null, we do not return valid value.
    // It will be ignored by loop since it will fail the test condition for loop
    if (!G->iter) return -1;
    G->visitedList[G->iter->node] = 1;
    return G->iter->node;
}


int done_neigh(Graph *G) {
    return G->iter == NULL;
}

int unvisited(Graph *G) {
    int i;
    // Find first unvisited node. Loop will halt at that point and we return
    for (i=0; i<G->numNodes && G->visitedList[i]; i++);
    if (i==G->numNodes) {
        return -1;
    }
    return i;
}

//free memory
void free_graph (Graph *g) {
    free(g->adjList);
    //free individual elements of linked list
    free(g->visitedList);
    free(g->iter);
}

double GetCurrWeight(Graph *G)
{
    return G->iter->weight;
}
