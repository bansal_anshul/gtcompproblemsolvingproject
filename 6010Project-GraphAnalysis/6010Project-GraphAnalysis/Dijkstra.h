//
//  Dijkstra.h
//  HW3_2 GraphAnalysis
//
//  Created by Devavret Makkar on 10/29/15.
//  Copyright © 2015 Devavret Makkar. All rights reserved.
//

#ifndef Dijkstra_h
#define Dijkstra_h

#include <stdio.h>
#include "Common.h"

struct Graph;

/**
 *  Stores the distances of every node from the given node. Stores the centrality betweenness for every node
 *
 *  @param G         The graph to use
 *  @param StartNode The node to start calculating distances from
 *  @param distances Array of node distances where the distance of node i from
 *                   StartNode is stored in distances[i]
 *  @param array     Array of betweenness values where the betweenness for node i
 *                   is stored in array[i]
 */
void Dijkstra(struct Graph *G, int StartNode, double *distances, double *array);

/**
 *  Checks whether given set has all values filled. Filled sets have no value =0
 *
 *  @param set  The array of int to check
 *  @param size Size of the input array set
 *
 *  @return Whether set is filled or not. Filled set will return non 0 integer
 */
BOOL IsSetFull(int *set, size_t NumNodes);

/**
 *  Node of minimum distance not already in set S
 *
 *  @param set       Set S
 *  @param distances node distances from strating node
 *  @param NumNodes  total number of nodes
 *
 *  @return Node in set ~S(=Q) of minimum distance from S
 */
int MinDistNode(int *set, double *distance, size_t NumNodes);

#endif /* Dijkstra_h */
