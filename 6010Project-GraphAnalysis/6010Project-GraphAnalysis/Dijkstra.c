//
//  Dijkstra.c
//  Modified and Leveraged from HW3_2 GraphAnalysis


//  Modified code to use Binary heaps for searching in Q rather than simple arrays
//  Employs Brandes algorithm to calculate Centrality Betweenness for nodes
//  http://algo.uni-konstanz.de/publications/b-fabc-01.pdf

#include "Dijkstra.h"
#include "GraphIter.h"
#include "PQHeap.h"
#include "StackArray.h"

#define Inf 10000.0

BOOL IsSetFull(int *set, size_t NumNodes)
{
    int prod = 1;
    for (size_t i = 0; i < NumNodes; i++) {
        prod *= set[i];
    }
    return (prod > 0)? TRUE:FALSE;
}

//int MinDistNode(int *set, double *distance, size_t NumNodes)
//{
//    int leastDist = INT32_MAX;
//    int leastDistNode = 0;
//    for (int i = 0; i < NumNodes; i++) {
//        if (!set[i]) {
//            if (distance[i] < leastDist) {
//                leastDist = distance[i];
//                leastDistNode = i;
//            }
//        }
//    }
//    return leastDistNode;
//}

void Dijkstra(Graph *G, int StartNode, double *distance, double *CB)
{
    size_t NumNodes = GetSize(G);
    
    //Stores "predecessors" of all node in an adjaceny list
    Graph *Pred;
    
    Pred = create_graph((int)NumNodes);
    
    //Stores nodes in set S using stack implementation
    stack *S;
    
    S = create_stack((int)NumNodes);
    
    //Stores number of paths from each vertex
    int sigma[NumNodes];
    
    //Setting all sigma to zero except for sourcenode
    for (int i = 0; i<NumNodes; i++) {
        sigma[i] = 0;
    }
    sigma[StartNode] = 1;
    
    
    //Stores vertex dependancies
    double delta[NumNodes];
    
    //Setting all dependancies to zero
    for (int j = 0; j<NumNodes; j++) {
        delta[j] = 0;
    }
    
    //Create Heap for set Q
    HeapQueue* Q = pqheap_create();
    
    // Setting all distances to a huge value
    for (size_t i = 0; i < NumNodes ; i++) {
        distance[i] = Inf;
    }
    // Setting StartNode value to 0
    distance[StartNode] = 0;

    for (size_t i = 0; i < NumNodes ; i++) {
        //void *data = (void*) i;
        pqheap_insert(Q, distance[i], (unsigned int) i);
    }

    
    // This array will keep track of what nodes have been included in set S
    // Any node not included is still in set Q
    int *setIncluded = (int*) malloc(sizeof(int) * NumNodes);
    
    // Set all nodes to be in Q
    // Node i transferred to S will have setIncluded[i] = 1
    for (size_t i = 0; i < NumNodes; i++) {
        setIncluded[i] = 0;
    }
    
    // While Q not empty
    while (pqheap_size(Q) > 0) {
        
        // vertex in Q with minimum u.d
        //int u = MinDistNode(setIncluded, distance, NumNodes);
        unsigned int u = pqheap_delete(Q);
        
        // Include the node with smallest distance into the set S
        setIncluded[u] = 1;
        
        //Mod-Add to stack also
        push_stack(S, u);
        
        for (int i = first_neigh(G, u); !done_neigh(G); i = next_neigh(G)) {
            // For vertices belonging to Q
            //if (i != -1) {
                
                if (!setIncluded[i]) {
                    if (distance[u] + GetCurrWeight(G) < distance[i]) {
                        distance[i] = distance[u] + GetCurrWeight(G);
                        pqheap_refresh(Q, distance[i], (unsigned int) i);
                    }
                }
                
                else {
                    //Mod-Add to dependancy array
                    if (distance[u] == GetCurrWeight(G) + distance[i]) {
                        sigma[u] = sigma[u] + sigma[i];
                        addElement(Pred, (unsigned) u,(unsigned) i, 1);
                    }
                }
            //}
        }
    }
    
    //Calculate betweenness
    while (stack_empty(S)) {
        int w = pop_stack(S);
        for (size_t i = first_neigh(Pred, w); !done_neigh(Pred); i = next_neigh(Pred)) {
            delta[i] = delta[i] + ((double)sigma[i]/(double)sigma[w]) * (1 + delta[w]);
            if (w != StartNode) {
                //Calculate betweenness recursively and normalize based on total possible number of edges
                //CB[w] = CB[w] + (double)delta[w]/((NumNodes-1)*(NumNodes-2)*2/2);
                CB[w] = CB[w] + delta[w]/2;
            }
        }
    }
    
    free_stack(S);
    
    free_graph(Pred);
    
    free(setIncluded);
    
    
    return;
    
}