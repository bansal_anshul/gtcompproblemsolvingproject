//
//  PQHeap.c
//  6010Assignment2-2
//
//  Created by Anshul Bansal on 10/6/15.
//  Copyright © 2015 Anshul Bansal. All rights reserved.
//

#include "PQHeap.h"
#include <stdlib.h>
#include "Common.h"
#include <math.h>

int MaxHeapSize = 10;
int elementnumber = 0;

// This file implements a MIN Priority Queue (i.e. elements with the
// smallest "priority" are removed first from the queue) using concept of Heaps implemented through an Array

HeapQueue growHeap (HeapQueue Array);

void heapify_up(int i, HeapQueue hq);

HeapQueue shrinkHeap (HeapQueue Array);

void heapify_down(int i, HeapQueue hq);



//Define one element of Priority Queue as struct
struct HeapNode {
    double priority;
    void* data;
};




//create priority queue as array of structs and return the array
HeapQueue pqheap_create() {
    HeapQueue hq;
    hq = (HeapQueue) malloc(sizeof(struct HeapNode)*10);
    if (hq == NULL) FatalError("pqheap_create", "Could not allocate memory.");
    return hq;
}

//insert element into priority queue
HeapQueue pqheap_insert(HeapQueue hq, double priority, void *data) {
    //check for empty priority queue
    if (hq == NULL) {
        return NULL;
    }
    
    //hq = (HeapQueue) realloc(hq, sizeof(struct HeapNode) * (elementnumber+1));
    
    //insert values of new element in last node
    hq[elementnumber].data = data;
    hq[elementnumber].priority = priority;
    
    //move this new element up the priority queue so that heap property is maintained
    heapify_up(elementnumber, hq);
    elementnumber ++;
    
    //If pq is full then increase the size of the queue
    if (elementnumber == MaxHeapSize) {
        HeapQueue temp;
        temp = growHeap(hq);
        free(hq);
        return temp;
    }
    return hq;

}

//Increase the size of the priority queue by creating a new array of double the original size and
//copying elements of original array to the new array
HeapQueue growHeap (HeapQueue Array) {
    //HeapQueue temp = (HeapQueue) realloc(*Array, ((elementnumber+1)*sizeof(HeapQueue)));
    
    //create new queue with double the size
    MaxHeapSize = MaxHeapSize*2;
    HeapQueue temp = (HeapQueue) malloc(sizeof(struct HeapNode)*MaxHeapSize);
    if (temp == NULL) {
        FatalError("pqheap_create", "Could not allocate memory.");
    }
    else {
        //copy elements
        for (int i = 0; i<elementnumber; i++) {
            temp[i].priority = Array[i].priority;
            temp[i].data = Array[i].data;
        }
    }
    return temp;
}

//Move the inserted item up the queue to maintain heap property
void heapify_up(int i, HeapQueue hq) {
    //if we are at the top of the heap then return
    if (i==0) {
        return;
    }
    //if priority of element is lower than parent then swap elements
    if (hq[i].priority<hq[(i-1)/2].priority) {
        struct HeapNode temp;
        temp.priority = hq[i].priority;
        temp.data = hq[i].data;
        hq[i].priority = hq[(i-1)/2].priority;
        hq[i].data = hq[(i-1)/2].data;
        hq[(i-1)/2].priority = temp.priority;
        hq[(i-1)/2].data = temp.data;
        heapify_up((i-1)/2, hq);
    }
    return;
}

//delete element from priority queue
HeapCombo pqheap_delete(HeapQueue hq) {
    HeapCombo temp;
    if (hq == NULL) {
        temp.data = NULL;
        temp.HQ = NULL;
        return temp;
    }
    //void* data = hq[0].data;
    //HeapCombo temp;
    temp.data = hq[0].data;
    temp.HQ = hq;
    if (elementnumber == 1) {
        elementnumber --;
        temp.HQ = NULL;
        free(hq);
        return temp;
    }
    //Remove root node of heap and copy data from last element into root node
    hq[0].priority = hq[elementnumber - 1].priority;
    hq[0].data  = hq[elementnumber - 1].data;
    
    //Set priority of last element to some large finite value
    hq[elementnumber-1].priority = 100000;
    hq[elementnumber-1].data = NULL;
    elementnumber --;
    
    //Move the root element down the queue to maintain heap property
    heapify_down(0, hq);
    
    //If current array is larger by more than twice the required heap, then create a new heap with 75% of the original size
    if (MaxHeapSize/2 > elementnumber) {
        HeapQueue temp2;
        temp2 = shrinkHeap(hq);
        free(hq);
        temp.HQ = temp2;
        return temp;
    }
    return temp;
}

//Reduce the size of the priority queue by creating a new array of 75% of the original size
//and copy the elements from original array to this new array
HeapQueue shrinkHeap (HeapQueue Array) {
    //HeapQueue temp = (HeapQueue) realloc(*Array, ((elementnumber+1)*sizeof(HeapQueue)));
    MaxHeapSize = (int) floor(MaxHeapSize*0.75);
    HeapQueue temp = (HeapQueue) malloc(sizeof(struct HeapNode)*MaxHeapSize);
    if (temp == NULL) {
        FatalError("pqheap_create", "Could not allocate memory.");
    }
    else {
        //copy elements in new array
        for (int i = 0; i<elementnumber; i++) {
            temp[i].priority = Array[i].priority;
            temp[i].data = Array[i].data;
        }
    }
    return temp;
}

//move the elements down the queue to maintain heap property
void heapify_down(int i, HeapQueue hq) {
    //if element does not have a child node
    if (2*i+1 > elementnumber-1) {
        return;
    }
    
    //if element has only one child node
    else if (2*i+1 == elementnumber - 1) {
        //if priority of parent is lower than child node
        if (hq[i].priority <= hq[2*i+1].priority) {
            return;
        }
        else {
            //swap parent and child
            struct HeapNode temp;
            temp.priority = hq[2*i+1].priority;
            temp.data = hq[2*i+1].data;
            hq[2*i+1].priority = hq[i].priority;
            hq[2*i+1].data = hq[i].data;
            hq[i].priority = temp.priority;
            hq[i].data = temp.data;
            return;
        }
        
    }
    
    //if priority of parent node is lower than both children nodes
    if (hq[i].priority <= hq[2*i+1].priority && hq[i].priority <= hq[2*i+2].priority) {
        return;
    }
    
    //else swap parent element with child element that has lowest priority
    if (hq[2*i+1].priority < hq[2*i+2].priority) {
        struct HeapNode temp;
        temp.priority = hq[2*i+1].priority;
        temp.data = hq[2*i+1].data;
        hq[2*i+1].priority = hq[i].priority;
        hq[2*i+1].data = hq[i].data;
        hq[i].priority = temp.priority;
        hq[i].data = temp.data;
        heapify_down(2*i+1, hq);
    }
    
    else {
        struct HeapNode temp;
        temp.priority = hq[2*i+2].priority;
        temp.data = hq[2*i+2].data;
        hq[2*i+2].priority = hq[i].priority;
        hq[2*i+2].data = hq[i].data;
        hq[i].priority = temp.priority;
        hq[i].data = temp.data;
        heapify_down(2*i+2, hq);
    }
    
}

//get required size of priority queue
unsigned int pqheap_size(HeapQueue pq) {
    if (pq == NULL)
        return 0;
    
    return elementnumber;
}

//free space from priority queue
void pqheap_free (HeapQueue hq) {
    if (hq == NULL)
        return;

    free(hq);
    
}

