//
//  PQHeap.h
//  6010Assignment2-2
//
//  Created by Anshul Bansal on 10/6/15.
//  Copyright © 2015 Anshul Bansal. All rights reserved.
//

#ifndef PQHeap_h
#define PQHeap_h

#include <stdio.h>

// This file implements a MIN Priority Queue (i.e. elements with the
// smallest "priority" are removed first from the queue) using concept of Heaps implemented through an Array

//Define a pointer to element
typedef struct HeapNode* HeapQueue;

//define a struct to store void* data and the priority queue in which the data is stored
typedef struct HeapCombo {
    HeapQueue HQ;
    void *data;
} HeapCombo;


/*
 * Create an empty priority queue as an array of structs
 *
 * @return the priority queue as an array
 */
HeapQueue pqheap_create();


/*
 * Push an element with a given priority into the queue.
 *
 * @param hq the priority queue
 * @param priority the element's priority
 * @param data the data representing our element
 */
HeapQueue pqheap_insert(HeapQueue hq, double priority, void *data);

/*
 * Pop the top element from the priority queue.
 *
 * @param hq the priority queue
 *
 * @return the data representing the top element and the priority queue as a struct
 */
HeapCombo pqheap_delete(HeapQueue hq);

/*
 * Free the memory allocated to the priority queue.
 *
 * @param hq the priority queue
 */
void pqheap_free (HeapQueue hq);

/*
 * Get the size of the priority queue
 *
 * @param hq the priority queue
 *
 * @return the size of the queue
 */
unsigned int pqheap_size(HeapQueue pq);



#endif /* PQHeap_h */
