//
//  TestPQ.h
//  6010Assignment2-2
//
//  Created by Anshul Bansal on 10/8/15.
//  Copyright © 2015 Anshul Bansal. All rights reserved.
//

#ifndef TestPQ_h
#define TestPQ_h

#include <stdio.h>

//Testing functions that evaluate the processing speed of different types of priority queues
//Each function creates a priority queue of either linked lists or heaps with initial size of NEvents events
//and then runs Iterations number of loops in which it deletes and adds one event
//The function finally gives out the numbe of iterations run divided by the wallclock time


//Function to test the event processing speed of Linked List based Priority Queue
//Takes initial size of queue and number of events to process as parameters
void TestHeapQueue (int NEvents, int Iterations);

//Function to test the event processing speed of Linked List based Priority Queue
//Takes initial size of queue and number of events to process as parameters
void TestLinkedQueue (int NEvents, int Iterations);

#endif /* TestPQ_h */
