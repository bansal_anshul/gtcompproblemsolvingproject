//
//  TestPQ.c
//  6010Assignment2-2
//
//  Created by Anshul Bansal on 10/8/15.
//  Copyright © 2015 Anshul Bansal. All rights reserved.
//

#include "TestPQ.h"
#include "PriorityQueue.h"
#include "Common.h"
#include "PQHeap.h"
#include <stdlib.h>
#include <math.h>
#include <time.h>


// declare simulation time
double TestHeapsimtime = 0;
double TestLinkedsimtime = 0;
double checksimtime;


PriorityQueue *TestLinkedFEL = NULL;      // Future Event List
HeapQueue TestFEL = NULL;      // Future Event List

// define container for simulations events
typedef struct SimEvent {
    double timestamp;					// event time stamp
    void *data;                         // application data
} TestSimEvent;


/***************** Random Number Generation *****************\
 *                                                          *
 * These functions define how to generate a random number   *
 * from an exponential distribution. Also defined are the   *
 * variables used as the means of the distribution.         *
 \************************************************************/


#define A1 1                     // mean of exponential distribution

/*
 * Generate a uniform random number in the range [0,1)
 */
double Testurand(void) {
    double x;
    while ((x = (rand() / (double) RAND_MAX)) >= 1.0);		// loop until x < 1 to exclude 1.0
    return x;
}

/*
 * Generate a random number from an exponential distribution
 * with a given mean.
 *
 * @param mean the mean of the distribution
 * @return a number draw from exponential distribution
 */
double Testrandexp(double mean) {
    return (-1 * mean) * (log(1.0 - Testurand()));
}

//Function to test the event processing speed of Heap based Priority Queue
//Takes initial size of heap and number of events to process as parameters
void TestHeapQueue (int NEvents, int Iterations) {
    
    srand((unsigned int)time(NULL));        // seed the random number generator
    
    for (int i = 0; i < NEvents; i++) {
    
        // Create the first element and insert it in the queue
        TestSimEvent *TestData = (TestSimEvent *) malloc (sizeof(TestSimEvent));
        if (TestData == NULL) FatalError("main", "Could not allocate memory for first event.");
        
        
        TestData->data = NULL;
        TestData->timestamp = TestHeapsimtime;
        TestHeapsimtime = TestHeapsimtime + Testrandexp(A1);
        
        // if we have not yet initialize a priority queue, create one
        if (TestFEL == NULL)
            //FEL = pq_create();
            TestFEL = pqheap_create();
        // pass the simulation event into the queue with priority equal to timestamp
        //pq_push(FEL, timestamp, se);
        TestFEL = pqheap_insert(TestFEL, TestData->timestamp, TestData);
        //printf("Event %i inserted with timestamp %f \n",i,TestData->timestamp);
        
        //free(TestData);
    }
    
    if (TestFEL == NULL)
        return;

    
    clock_t start, end;
    start = clock();        // measures the "time" now
    
    //For n=iterations, keep deleting and adding events to the priority queue
    for (int j = 0; j < Iterations; j++) {
        
        //printf("Current size of heap is = %i \n",pqheap_size(TestFEL));
        
        //Remove one event from priority queue
        HeapCombo HC = pqheap_delete(TestFEL);
        TestFEL = HC.HQ;
        //TestSimEvent *se = (TestSimEvent *) HC.data;
        //checksimtime = se->timestamp;
        
        //printf("Current size of heap is = %i \n",pqheap_size(TestFEL));
        //printf("Current timestamp of popped event is = %3.3f \n",checksimtime);
        
        //Create new event
        TestSimEvent *TestData = (TestSimEvent *) malloc (sizeof(TestSimEvent));
        if (TestData == NULL) FatalError("main", "Could not allocate memory for first event.");
        
        TestData->data = NULL;
        TestData->timestamp = TestHeapsimtime;
        TestHeapsimtime = TestHeapsimtime + Testrandexp(A1);
        
        if (TestFEL == NULL)
            FatalError("test queue", "Empty mid run");
       
        // pass the simulation event into the queue with priority equal to timestamp
        TestFEL = pqheap_insert(TestFEL, TestData->timestamp, TestData);
        free(TestData);

    }
    end = clock();
    
    clock_t diff = end - start;     // compute time elapsed
    
    float timeElapsed = diff / (float) CLOCKS_PER_SEC;          // covert to seconds
    
    // print time elapsed
    printf("\nTest Heaps: Initial size of priority queue = %i \n",NEvents);
    //printf("Events processed = %i\n", Iterations);
    //printf("Wallclock time = %.6f (s)\n", timeElapsed);
    printf("Events processed per wallclock time = %.6f\n",Iterations/timeElapsed);
    return;
    
}

//Function to test the event processing speed of Linked List based Priority Queue
//Takes initial size of queue and number of events to process as parameters
void TestLinkedQueue (int NEvents, int Iterations) {
    
    srand((unsigned int)time(NULL));        // seed the random number generator
    
    for (int i = 0; i < NEvents; i++) {
        
        // Create the first element and insert it in the queue
        TestSimEvent *TestData = (TestSimEvent *) malloc (sizeof(TestSimEvent));
        if (TestData == NULL) FatalError("main", "Could not allocate memory for first event.");
        
        
        TestData->data = NULL;
        TestData->timestamp = TestLinkedsimtime;
        TestLinkedsimtime = TestLinkedsimtime + Testrandexp(A1);
        
        // if we have not yet initialize a priority queue, create one
        if (TestLinkedFEL == NULL)
            TestLinkedFEL = pq_create();
    
        // pass the simulation event into the queue with priority equal to timestamp
        pq_push(TestLinkedFEL, TestData->timestamp, TestData);
        
        //printf("Event %i inserted with timestamp %f \n",i,TestData->timestamp);
        
        //free(TestData);
    }
    
    if (TestLinkedFEL == NULL)
        return;
    
    clock_t start, end;
    start = clock();        // measures the "time" now
    
    //For n=iterations, keep deleting and adding events to the priority queue
    for (int j = 0; j < Iterations; j++) {
        
        //printf("Current size of heap is = %i \n",pqheap_size(TestFEL));
        
        //Remove lowest priority event from priorty queue
        TestSimEvent *se = (TestSimEvent *) pq_pop(TestLinkedFEL);
        checksimtime = se->timestamp;
        
        //printf("Current timestamp of popped event is = %3.3f \n",checksimtime);
        
        //free(se);
        
        //Create a new event
        TestSimEvent *TestData = (TestSimEvent *) malloc (sizeof(TestSimEvent));
        if (TestData == NULL) FatalError("main", "Could not allocate memory for first event.");
        
        TestData->data = NULL;
        TestData->timestamp = TestLinkedsimtime;
        TestLinkedsimtime = TestLinkedsimtime + Testrandexp(A1);
        
        if (TestLinkedFEL == NULL)
            FatalError("test queue", "Empty mid run");
        
        // pass the simulation event into the queue with priority equal to timestamp
        pq_push(TestLinkedFEL, TestData->timestamp, TestData);
        free(TestData);
        
    }
    end = clock();
    
    clock_t diff = end - start;     // compute time elapsed
    
    float timeElapsed = diff / (float) CLOCKS_PER_SEC;          // covert to seconds
    
    // print time elapsed
    printf("\nTest Linked Lists: Initial size of priority queue = %i \n",NEvents);
    //printf("Events processed = %i\n", Iterations);
    //printf("Wallclock time = %.6f (s)\n", timeElapsed);
    printf("Events processed per wallclock time = %.6f\n",Iterations/timeElapsed);
    return;
    
}
