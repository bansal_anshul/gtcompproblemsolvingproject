//
//  main.c
//  CORAGraphGen
//
//  Created by Anshul Bansal on 12/1/15.
//  Copyright © 2015 Anshul Bansal. All rights reserved.
//

//This program takes a gml graph dataset as input and outputs two text files
//One text file contains node info, i.e. node id and name
//Second file contains edge list and weights

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>


int main(int argc, const char * argv[]) {
    
    //Open file and read contents
    FILE *fp = fopen(argv[1], "r");
    if (fp == NULL) {
        printf("Cant open file. Please resolve error\n");
        return 1;
    }
    
    
    //Write node id and names to a file
    char nodeinfo[50];
    sprintf(nodeinfo, "%s_ninfo", argv[1]);
    FILE *fp2 = fopen(nodeinfo, "w");
    fprintf(fp2, "Node Name\n");
    
    //Write node id and names to a file
    char edgeinfo[50];
    sprintf(edgeinfo, "%s_einfo", argv[1]);
    FILE *fp3 = fopen(edgeinfo, "w");
    //fprintf(fp3, "Node Name\n");
    
    int flag = 0;
    int from = -1;
    int to = -1;
    float weight = -1.0;
    
    
    //Get number of nodes and call function to create adjacency list
    //char word[100];
    int countNodes = 0;
    int node;
    char target[100], label[10], fname[30], sname[1];
    //char fnamearray[1600][30];
    //char snamearray[1600][1];
    fscanf(fp, "%s",target);
    
    //strcpy(id,"id");
    while (!feof(fp)) {
        fscanf(fp, "%s",target);
        if (!strcmp(target,"id")) {
            countNodes++;
 
            fscanf(fp, "%d",&node);
            fscanf(fp, "%s %s %s",label,fname,sname);

            fprintf(fp2, "%d %s %s\n",countNodes-1,fname,sname);
            
        }
        
        else if (!strcmp(target, "source")) {
            if (flag==0) {
                fprintf(fp3, "%d\n",countNodes);
                flag=1;
            }
            fscanf(fp, "%d",&from);
        }
        
        else if (!strcmp(target, "target")) {
            fscanf(fp, "%d",&to);
        }
        
        else if (!strcmp(target, "value")) {
            fscanf(fp, "%f",&weight);
            fprintf(fp3, "%d %d %f\n",from,to,weight);
        }

    }
    
    //Close file
    fclose(fp);
    fclose(fp2);
    fclose(fp3);
    
    printf("%d",countNodes);
    //printf("%s",snamearray[1587]);

    
    // All well
    return 0;
}
